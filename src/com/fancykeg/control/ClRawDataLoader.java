package com.fancykeg.control;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.fancykeg.adapter.FancyAdapter;
import com.fancykeg.to.CraigsListPost;
import com.fancykeg.to.CraigsListTO;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import static java.util.Arrays.*;
import static com.fancykeg.util.FancyParse.*;

public class ClRawDataLoader {

	private static final String TAG = ClRawDataLoader.class.getName();
	private static final int THREAD_POOL = 3;

	/**
	 * Starts a process that will pull raw data for clist
	 * @param clTo
	 * @throws InterruptedException 
	 */
	public void populateClistData(CraigsListTO clTo, FancyAdapter fancyAdapter) {
		//thread will start more threads
		ExecutorService es = Executors.newFixedThreadPool(1);
		es.execute(new DataLoader(clTo, fancyAdapter));
	}

	/**
	 * Loads process the raw craigslist data
	 * @author matt
	 */
	public class DataLoader implements Runnable {
		CraigsListTO clTo;
		FancyAdapter fancyAdapter;
		ExecutorService es; 

		public DataLoader(CraigsListTO clTo, FancyAdapter fancyAdapter) {
			this.clTo = clTo;
			this.fancyAdapter = fancyAdapter;
			es = Executors.newFixedThreadPool(THREAD_POOL); 
		}

		@Override
		public void run() {

			URL url;
			InputStream is = null;
			BufferedReader br;
			String line = "";

			Collection<Future<CraigsListPost>> futures = new ArrayList<Future<CraigsListPost>>();

			try {
				url = new URL(clTo.getRequest());
				is = url.openStream();  // throws an IOException
				br = new BufferedReader(new InputStreamReader(is));

				while ((line = br.readLine()) != null) {
					line = line.trim();
					if(line.startsWith("<p"))	{
						futures.add(es.submit(new DataProcessor(line, clTo)));
					}
				}

			} catch (MalformedURLException mue) {
				mue.printStackTrace();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} finally {
				try {
					if (is != null) { is.close(); }
				} catch (IOException ioe) {
					// nothing to see here
				}
			}

			for (Future<CraigsListPost> future : futures) {
				try {
					CraigsListPost clPost = future.get();

					//remove the Element object
					if(clPost != null) {
						clTo.getClPosts().put(clPost.getDataPid(), clPost);
						fancyAdapter.addItem(clPost);
					}

				} catch (InterruptedException e) {
					e.printStackTrace();

				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}
		}
	} //DataLoader


	/*
	 * TODO see if we can't log how many characters we are skipping over on the 
	 * findNext methods
	 */
	public class DataProcessor implements Callable<CraigsListPost> {
		String rawPost;
		CraigsListTO clTo;

		public DataProcessor(String rawPost, CraigsListTO clTo) {
			this.rawPost = rawPost;
			this.clTo = clTo;
		}

		@Override
		public CraigsListPost call() throws Exception {
			CraigsListPost clPost = new CraigsListPost();
			char[] rawChar = rawPost.toCharArray();			

			try {

				//no picture
				if(findNextSequence(rawChar, "=\"p\"> pic", 0) < 0){
					return null;
				}

				int s = "<p class=\"row\" data-".length();
				int e = s;

				//if l - we have lat and longitude
				if(rawChar[s] == 'l'){

					s = s + "latitude=\"".length();
					e = findNextChar(rawChar, '"', s);
					clPost.setLatitude(String.valueOf(copyOfRange(rawChar, s, e)));

					s = e+1 + " data-longitude=\"".length();
					e = findNextChar(rawChar, '"', s);
					clPost.setLongitude(String.valueOf(copyOfRange(rawChar, s, e)));

					s = e+1 + " data-pid=\"".length();
					e = findNextChar(rawChar, '"', s);
				} 

				//otherwise no GPS - data-pid only
				if(clPost.getLatitude() == null){
					s = e+1 + "id=\"".length();
					e = findNextChar(rawChar, '"', s);
				} 

				clPost.setDataPid(String.valueOf(copyOfRange(rawChar, s, e)));

				s = e+1 + "> <a href=\"".length();
				e = findNextChar(rawChar, '"', s);

				clPost.setPostLink(clTo.getBaseUrl()+String.valueOf(copyOfRange(rawChar, s, e)));

				//s will be negative when no price returned
				s = findNextSequence(rawChar, "class=\"price\">", e);
				if(s > 0) { 
					s = s + "class=\"price\">".length();
					s = 1+findNextChar(rawChar, ';', s);
					e = findNextChar(rawChar, '<', s);
					clPost.setPrice(String.valueOf(copyOfRange(rawChar, s, e)));
				} else {
					Log.w(TAG, "price not found - " + rawPost);
					//stop execution
					return null;
				}

				s = findNextSequence(rawChar, "class=\"date\">", e);
				s = s + "class=\"date\">".length();
				e = findNextChar(rawChar, '<', s);
				clPost.setDate(String.valueOf(copyOfRange(rawChar, s, e)));

				s = findNextSequence(rawChar, ".html\">", e);
				s = s + ".html\">".length();
				e = findNextSequence(rawChar, "</a>", s);
				clPost.setTitle(String.valueOf(copyOfRange(rawChar, s, e)));

				s = findNextSequence(rawChar, "<small>", e);
				if(s > 0) {
					s = s + "<small>".length();
					e = findNextSequence(rawChar, "</small>", s);
					clPost.setDesc(String.valueOf(copyOfRange(rawChar, s, e)));
				} 
				
				clPost.setPictureLinks(getPictureLinks(clPost.getPostLink()));
				clPost.setBitmap(getPostPicture(clPost.getPictureLinks().get(0)));
				
				if(clPost.getBitmap() == null){
					return null;
				}
			}

			//catch any problems from parsing a row
			catch(Exception e){
				Log.e(TAG, "parsing failed - " + rawPost, e);
				Log.e(TAG, clPost.toString());
				return null;
			}			

			return clPost;
		}

	}

	private static Bitmap getPostPicture(String link) {
		Bitmap bitmap = null;
		InputStream in = null;
		try {
			in = new URL(link).openStream();
			bitmap = BitmapFactory.decodeStream(in);
		} catch (Exception e) {
			Log.e("Error", e.getMessage());
			e.printStackTrace();
		}
		return bitmap;
	}


	private static List<String> getPictureLinks(String Url) throws MalformedURLException, IOException{
		List<String> pictureLinks = new ArrayList<String>();

		URL url = new URL(Url);
		InputStream is = url.openStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line = "";

		while((line = br.readLine()) != null){
			line = line.trim();
			if(line.startsWith("imgList")){
				String[] dirtyLinks = line.split(",");

				int i = 0;
				while(i < dirtyLinks.length){
					String link = dirtyLinks[i];

					int s = 1 + findNextChar(link.toCharArray(), '"', 0);
					int e;

					//last one
					if(i == (dirtyLinks.length - 1)) {
						e = link.length() - 3;						
					} else {
						e = link.length() - 1;
					}

					pictureLinks.add(link.substring(s, e));
					i++;
				}
			}
		}

		if(br != null) { br.close(); }

		return pictureLinks;
	}

	/**
	 * Provides a data processor for testing and what not
	 * @param input
	 * @param clTo
	 * @return
	 */
	public DataProcessor dataProcessorFactory(String input, CraigsListTO clTo) {
		return new DataProcessor(input, clTo);
	}


}
