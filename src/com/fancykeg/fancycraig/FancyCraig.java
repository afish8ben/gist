package com.fancykeg.fancycraig;

import com.fancykeg.to.CraigsListPost;
import com.fancykeg.to.CraigsListTO;
import com.fancykeg.util.SwipeDismissListViewTouchListener;
import com.fancykeg.adapter.FancyAdapter;

import android.os.Bundle;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class FancyCraig extends ListActivity {

	private static final String TMP_BASEURL = "http://atlanta.craigslist.org";
	private static final String TMP_CATEGORY = "/sss";
	
	private FancyAdapter fAdapter;
	private ListView fList;	
	private String location;
	private Account[] accounts;
	private AlertDialog accountPopup;
	private int prevPosition = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fancy_craig);		
		
		fList = (ListView)findViewById(android.R.id.list);
		
		fAdapter = new FancyAdapter(this, new CraigsListTO(TMP_BASEURL, TMP_CATEGORY));
		setListeners(fAdapter);

		Intent i = getIntent();
		location = i.getStringExtra("location");
		Toast.makeText(getApplicationContext(), "Current Location: " + location , Toast.LENGTH_LONG).show();
	}

	@Override
	protected void onStart(){
		super.onStart();		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.	
		getMenuInflater().inflate(R.menu.activity_fancy_craig, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		case R.id.menu_login:
			displayAccounts();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void setListeners(FancyAdapter fAdapter2) {
		fList.setAdapter(fAdapter);		
		SwipeDismissListViewTouchListener listener = new SwipeDismissListViewTouchListener(
				fList, new SwipeDismissListViewTouchListener.DismissCallbacks() {							

					@Override
					public void onDismiss(ListView listView, int[] reverseSortedPositions) {
						for(int position : reverseSortedPositions){
							fAdapter.remove(position);
						}
						fAdapter.notifyDataSetChanged();
					}							

					@Override
					public boolean canDismiss(int position) {								
						return true;
					}
				});

		fList.setOnTouchListener(listener);
		fList.setOnScrollListener(listener.makeScrollListener());				
	}

	@Override
	public void onListItemClick(ListView list, View v, int position, long id){
		if(getString(R.string.Environment) == "DEBUG")
			Log.i("FancyCraig", "You clicked item at position: " + position);

		if(position == prevPosition){
			CraigsListPost listing = (CraigsListPost) fAdapter.getItem(position);	
			listing.setLiked(true);
			fAdapter.updateItem(position, listing);
			fAdapter.notifyDataSetChanged();	
			ImageView tView = (ImageView) v.findViewById(R.id.like); 
			tView.setImageResource(R.drawable.green_check_mark);
			tView.bringToFront();
			prevPosition = -1;
		}else{
			prevPosition = position;
		}
	}	

	private void displayAccounts(){				
		AccountManager accManager = AccountManager.get(getApplicationContext());
		accounts = accManager.getAccountsByType("com.google");
		String[] acc_names = new String[accounts.length];
		for (int i = 0; i < accounts.length; i++) {
			acc_names[i] = accounts[i].name;
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Select Account");				
		builder.setAdapter(new ArrayAdapter<String>(FancyCraig.this, R.layout.account_item,
				acc_names), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				//control.setAccount(accounts[which]);				
			}
		});
		accountPopup = builder.create();
		accountPopup.show();
	}

}
