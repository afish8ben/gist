package com.fancykeg.fancycraig;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

public class SplashView extends Activity {	
	private static final int SPLASH_TIME = 1000;	
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		new Handler().postDelayed(new Runnable(){
			@Override
			public void run() {
				String location = getLocation();
				Intent i = new Intent(SplashView.this, FancyCraig.class);
				i.putExtra("location", location);
				startActivity(i);
				finish();
			}			
		}, SPLASH_TIME);
	}
	
	private String getLocation(){
		LocationManager locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		Criteria crit = new Criteria();
		String provider = locManager.getBestProvider(crit, false);
		Location location = locManager.getLastKnownLocation(provider);
		if(location != null){
			double latitude = location.getLatitude();
			double longitude = location.getLongitude();
			Geocoder geo = new Geocoder(this, Locale.getDefault()); 
			StringBuilder sb = new StringBuilder();
			try{
				List<Address> address = geo.getFromLocation(latitude, longitude, 1);
				int maxLines = address.get(0).getMaxAddressLineIndex();
				for(int i = 0; i < maxLines; i++){
					String a = address.get(0).getAddressLine(i);
					sb.append(a);
					sb.append(" ");
				}
				return sb.toString();
			}catch(IOException ex){
				Log.e("SplashView", "IOException thrown somethings just suck :/");
			}catch(NullPointerException ex){
				Log.e("SplashView", "NullPointerException thrown somethings just suck :/");
			}
		}else
			Toast.makeText(getApplicationContext(), "Unable to get Location", Toast.LENGTH_SHORT).show();				
		return null;
	}

}
