package com.fancykeg.to;
import com.google.gdata.data.IPerson;
public class UserTO {	
	String externalId;
	IPerson person;		
	public UserTO() {
	}
	public UserTO(String externalId, IPerson person){
		this.externalId = externalId;
		this.person = person;		
	}
}