package com.fancykeg.to;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import org.jsoup.select.Elements;

public class CraigsListTO implements Serializable {
	private static final long serialVersionUID = -8295433719916308757L;
	
	/*
	 * TODO when more advanced search functionality is added that data will held here
	 * TODO constructors should always build the request from their respective components
	 */
	
	private String baseUrl;
	private String category;
	private String request;
	private Map<String, CraigsListPost> clPosts = new ConcurrentHashMap<String, CraigsListPost>();

	/**
	 * Builds a request for a simple categor pull
	 * @param baseUrl
	 * @param category
	 */
	public CraigsListTO(String baseUrl, String category) {
		this.baseUrl = baseUrl;
		this.category = category;
		this.request = baseUrl + category;		
	}
	
	public String getBaseUrl() {
		return baseUrl;
	}

	public String getCategory() {
		return category;
	}	
	
	public String getRequest(){
		return request;
	}

	public Map<String, CraigsListPost> getClPosts() {
		return clPosts;
	}

	public void setClPosts(Map<String, CraigsListPost> clPosts) {
		this.clPosts = clPosts;
	}
	
}
