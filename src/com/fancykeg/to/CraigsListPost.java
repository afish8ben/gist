package com.fancykeg.to;

import java.io.Serializable;
import java.util.List;

import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.TextView;

public class CraigsListPost implements Serializable {
	private static final long serialVersionUID = -589399596922884298L;
	
	private String dataPid;
	private String postLink; 
	private String price;
	private String latitude;
	private String longitude;
	private String title;
	private String desc;
	private List<String> pictureLinks;
	private boolean liked = false;;
	private int index;
	private Bitmap bitmap;
	private String date;
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public CraigsListPost(){}

	public String getDataPid() {
		return dataPid;
	}

	public void setDataPid(String dataPid) {
		this.dataPid = dataPid;
	}

	public String getPostLink() {
		return postLink;
	}

	public void setPostLink(String postLink) {
		this.postLink = postLink;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public List<String> getPictureLinks() {
		return pictureLinks;
	}

	public void setPictureLinks(List<String> pictureLinks) {
		this.pictureLinks = pictureLinks;
	}

	public boolean isLiked() {
		return liked;
	}

	public void setLiked(boolean liked) {
		this.liked = liked;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataPid == null) ? 0 : dataPid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CraigsListPost other = (CraigsListPost) obj;
		if (dataPid == null) {
			if (other.dataPid != null)
				return false;
		} else if (!dataPid.equals(other.dataPid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CraigsListPost\n\tdataPid=");
		builder.append(dataPid);
		builder.append("\n\tpostLink=");
		builder.append(postLink);
		builder.append("\n\tprice=");
		builder.append(price);
		builder.append("\n\tlatitude=");
		builder.append(latitude);
		builder.append("\n\tlongitude=");
		builder.append(longitude);
		builder.append("\n\ttitle=");
		builder.append(title);
		builder.append("\n\tdesc=");
		builder.append(desc);
		builder.append("\n\tpictureLinks=");
		builder.append(pictureLinks);
		builder.append("\n\tliked=");
		builder.append(liked);
		builder.append("\n\tindex=");
		builder.append(index);
		builder.append("\n\tbitmap=");
		builder.append(bitmap);
		builder.append("\n\tdate=");
		builder.append(date);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @return the bitmap
	 */
	public Bitmap getBitmap() {
		return bitmap;
	}

	/**
	 * @param bitmap the bitmap to set
	 */
	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}
	

}
