package com.fancykeg.util;

import java.util.Arrays;

public class FancyParse {
	
	public static int findNextChar(char[] array, char c, int start){
		int i = start;
		while(i < array.length){
			if(array[i] == c){
				return i;
			}
			i++;
		}
		return -1;
	}
	
	public static int findNextSequence(char[] array, String seq, int start) {
		int i = start;
		char[] s = seq.toCharArray();  
		while(i < array.length){
			if(array[i] == s[0]){
				int j = 1;
				boolean equal = true;
				while(j < s.length && i+j<array.length){
					if(array[i+j] != s[j]){
						equal = false;
						break;
					}
					j++;
				}
				if(equal){
					return i;
				}
			}
			
			i++;			
		}
		
		return -1;
	}
}
