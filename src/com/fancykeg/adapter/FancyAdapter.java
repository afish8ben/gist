package com.fancykeg.adapter;

import java.util.ArrayList;
import java.util.List;

import com.fancykeg.control.ClRawDataLoader;
import com.fancykeg.fancycraig.R;
import com.fancykeg.to.CraigsListPost;
import com.fancykeg.to.CraigsListTO;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FancyAdapter extends BaseAdapter{

	private List<CraigsListPost> clPostings; 
	private static LayoutInflater inflater = null;
	private ClRawDataLoader rawDataCtrl = new ClRawDataLoader();
	private Activity activity;

	public FancyAdapter(Activity activity, CraigsListTO clTo){
		this.activity = activity;
		clPostings = new ArrayList<CraigsListPost>();
		rawDataCtrl.populateClistData(clTo, this);
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);				
	}

	public void remove(int position){
		clPostings.remove(position);		
	}
	
	public void updateItem(int position, CraigsListPost post){
		clPostings.set(position, post);
	}	
	@Override
	public int getCount() {
		return clPostings.size();
	}

	@Override
	public Object getItem(int position) {
		return clPostings.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	/**
	 * Allows us to dynamically expand our list
	 * @param clPost
	 */
	public void addItem(CraigsListPost clPost){
		activity.runOnUiThread(new LazyAdder(clPost, this));
	}
	
	/**
	 * Removes elements from our list
	 * @param clPost
	 */
	public void removeItem(CraigsListPost clPost){
		activity.runOnUiThread(new LazyRemover(clPost, this));
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if(convertView == null) {
			vi = inflater.inflate(com.fancykeg.fancycraig.R.layout.fancy_item , null);
		}

		//bg.setBackgroundResource(com.fancykeg.fancycraig.R.color.BackGround);
		CraigsListPost clPost = clPostings.get(position);

		TextView title = (TextView)vi.findViewById(R.id.txtTitle);
		TextView desc = (TextView)vi.findViewById(com.fancykeg.fancycraig.R.id.itemDescription);
		ImageView img = (ImageView)vi.findViewById(com.fancykeg.fancycraig.R.id.fancyimg);					
		
		title.setText(clPost.getTitle());
		desc.setText(clPost.getDesc());	
		img.setImageBitmap(clPost.getBitmap());

		if(clPost.isLiked()){
			ImageView liked = (ImageView)vi.findViewById(com.fancykeg.fancycraig.R.id.like);
			liked.setImageResource(R.drawable.green_check_mark);
			liked.bringToFront();
		}
		
		return vi;
	}
	
	private class LazyAdder implements Runnable {
		
		CraigsListPost clPost;
		FancyAdapter lazy;
		
		public LazyAdder(CraigsListPost clPost, FancyAdapter lazy) {
			this.clPost = clPost;
			this.lazy = lazy;
		}
		
		@Override
		public void run() {
			clPostings.add(clPost);
			lazy.notifyDataSetChanged();			
		}		
	}
	
	private class LazyRemover implements Runnable {
		CraigsListPost clPost;
		FancyAdapter lazy;
		
		public LazyRemover(CraigsListPost clPost, FancyAdapter lazy) {
			this.clPost = clPost;
			this.lazy = lazy;
		}
		
		@Override
		public void run() {
			clPostings.remove(clPost);
			lazy.notifyDataSetChanged();			
		}		
	}

}
