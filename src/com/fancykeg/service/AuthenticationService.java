package com.fancykeg.service;

import java.io.IOException;
import java.net.URL;

import com.fancykeg.to.UserTO;
import com.google.gdata.client.contacts.*;
import com.google.gdata.data.IFeed;
import com.google.gdata.data.contacts.ContactFeed;
import com.google.gdata.util.ServiceException;

public class AuthenticationService {
	
	private static String gUrl = "https://www.google.com/m8/feeds/contacts/default/full?max-results=10000";
	
	public static UserTO getUserId(String token){
		UserTO result;
		ContactsService cService = new ContactsService("Taxi");
		cService.setUserToken(token);
		IFeed feed = null;		
		try{
			feed = cService.getFeed(new URL(gUrl), ContactFeed.class);
		}catch(IOException e){
			e.printStackTrace();
		}catch(ServiceException e){
			e.printStackTrace();
		}		
		if(feed == null)
			return null;		
		result = new UserTO(feed.getId(), feed.getAuthors().get(0));
		return result;				
	}

}
