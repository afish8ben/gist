package com.fancykeg.exception;

public class ResolveResourceException extends Exception {

	private static final long serialVersionUID = -4759763277319241560L;

	public ResolveResourceException(String message){
		super(message);
	}
	
	public ResolveResourceException(String message, Throwable t){
		super(message, t);
	}
}
